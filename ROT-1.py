characters = (input('Enter 3 characters: ')).upper()
if len(characters) == 3 :
    cha = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    world = ''
    for x in characters :
        fincha = cha.find(x)
        if fincha<25 :
            world += cha[fincha+1]
        else :
            world += cha[0]
    print(f'Ciphertext is {world}')
else:
    print('word length is mismatched')